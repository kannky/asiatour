# -*- coding:utf-8 -*-

'''
Created on 2010. 6. 16.

@author: kann
'''
from __future__ import division
from django.http import HttpResponse
import math, datetime, uuid
from django.shortcuts import render_to_response
from django.template.context import RequestContext
#from django.utils.encoding import smart_unicode
#from django.utils.safestring import mark_safe
#from django.core import validators

#from smartMV.common.models import AModel
#from django.db import models

#from django.forms.util import ValidationError as FormValidationError

import json



from urlparse import urlparse 

from django.http import QueryDict
import os
import urllib
import calendar
import dateutil



def convert_error_message( eform ):
    
    return [  (eform.fields[error[0]].label , error[1] ) for error in eform.errors.items() ] 



#
#
def  verify_juminno ( number ):

    if len( number ) != 13 :
        return False

    nla = []
    s = 0
    for x in list( number ):
        if x in '0123456789':
            nla.append( x )
        if len( nla ) == 13:
            break
    p = ( 2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5 )
    for x in range( 12 ):
        s = s + int( nla[x] ) * int( p[x] )

    m = ( 11 - ( s % 11 ) ) % 10

    return m == int( nla[-1] ) and True or False








def set_cookie(response, key, value, clear = False):

    

    if clear :
        expires = None
        max_age = None  #one year
        
    else :
        max_age = 365*24*60*60  #one year
        expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
         
        
    response.set_cookie(key, value, max_age=max_age, expires=expires )

    return response




def session_required(f):
    def wrap(request, *args, **kwargs):
        request.member = request.session['ssUser']
        return f(request, *args, **kwargs)
    wrap.__doc__=f.__doc__
    wrap.__name__=f.__name__
    return wrap






def json_response(x):
    
    """
    return json_response({ 'success' : False,
         'errors' : [(k, v[0].__unicode__()) for k, v in form.errors.items()] })
    
    """
    
    return HttpResponse(json.dumps(x, sort_keys=False, indent=2),
                        content_type='application/json; charset=UTF-8')



def is_outsite( site ):


    if site in ['www.icarinsu.com', 'icarinsu.com', 'www.icarinsu.co.kr','icarinsu.co.kr',
                 'www.icarinsu.kr', 'icarinsu.kr', '127.0.0.1:8000', '127.0.0.1:8002' ] :
        return False
        

    return True 




def add_one_month(t):
    """Return a `datetime.date` or `datetime.datetime` (as given) that is
    one month earlier.
    
    Note that the resultant day of the month might change if the following
    month has fewer days:
    
        >>> add_one_month(datetime.date(2010, 1, 31))
        datetime.date(2010, 2, 28)
    """

    one_day = datetime.timedelta(days=1)
    one_month_later = t + one_day
    while one_month_later.month == t.month:  # advance to start of next month
        one_month_later += one_day
    target_month = one_month_later.month
    while one_month_later.day < t.day:  # advance to appropriate day
        one_month_later += one_day
        if one_month_later.month != target_month:  # gone too far
            one_month_later -= one_day
            break
    return one_month_later






def subtract_one_month(t):
    
    """
    Return a `datetime.date` or `datetime.datetime` (as given) that is
    one month later.
    
    Note that the resultant day of the month might change if the following
    month has fewer days:
    
        >>> subtract_one_month(datetime.date(2010, 3, 31))
        datetime.date(2010, 2, 28)
    """
    

    one_day = datetime.timedelta(days=1)
    one_month_earlier = t - one_day
    while one_month_earlier.month == t.month or one_month_earlier.day > t.day:
        one_month_earlier -= one_day
    return one_month_earlier






def getPageCount(totalCount=None, pageMax=None):
    if totalCount == None or pageMax == None: return 1
    else: return int( math.ceil( float(totalCount) / float( pageMax ) ) )




def row_evalstr( elements , rowname ):
    ss = ""
    for element in elements : 
        ss += " %(s)s.{0}, ".format(element)
    return   ss  % {'s': str(rowname)  } 




def query_list(elements, query, pageMax, pageNum):
    
    #return [  eval(row_evalstr(elements, 'row'))  for row in query[(pageNum - 1) * pageMax:(pageNum - 1) * pageMax + pageMax]  ]

    if pageMax == None or pageNum == None: 
        return [ eval(row_evalstr(elements, 'row') ) for row in  query ]
    
    else :
        rtnval = [  eval(row_evalstr(elements, 'row'))  for row in query[(pageNum - 1) * pageMax:(pageNum - 1) * pageMax + pageMax]  ]
    
        if len(rtnval) == 0 and  query.count() > 0 :
            pageNum = 1
            return [  eval(row_evalstr(elements, 'row'))  for row in query[(pageNum - 1) * pageMax:(pageNum - 1) * pageMax + pageMax]  ]
        return rtnval



#from django import forms
#이방원 
def get_query_list(modetype,curpage,lineMax,pageMax,modelobject,ordertuple,**kwargs):

    '''
    curpage 현재 페이지
    lineMax 한페이지당 최대 줄수
    pageMax 한 화면당 최대 페이지링크 예 pageMax=3  
        ex) <pre 1  2  3  next> or <pre 7  8  9  next>
    modelobject db모델
    ordertuple orderby 변수
    **kwargs (id=1,name="d") filter용 
    return isVal,STARTPAGE,ENDPAGE,TOTALPAGE,listdata
    isVal 현제페이지를 기준으로 맞는지
    STARTPAGE <pre 1  2  3  next>에서 1
    ENDPAGE  <pre 1  2  3  next>에서 3
    get_query_list(2,3,2,Project,('id'),name='12')
    
    
    
    python manage.py shell
    from joc.common.utils import *
    from affair.models import *
    get_query_list(2,3,2,Project,[],name='12')
    
    from django.db.models import Q
    
    modetype ='1' or 'and'
    modetype ='2' or 'or'
    
    '''
    isVal=True
    if lineMax==0 or curpage==0:
        isVal=False
        lineMax=1
        curpage=1
        pass
    OFFSET=(curpage-1)*lineMax
    LIMIT=OFFSET+lineMax
    
    TOTALCOUNT=modelobject.objects.filter(**kwargs).count()
    TOTALPAGE=TOTALCOUNT//lineMax
    
    listdata=None
    
    if(TOTALCOUNT%lineMax!=0 or TOTALCOUNT==0):
        TOTALPAGE+=1
        pass
    if(TOTALPAGE<curpage):
        isVal=False
        pass
    
    
    if isVal :
        listdata=modelobject.objects.filter(**kwargs).order_by(*ordertuple)[OFFSET:LIMIT]
    
    ENDPAGELIMIT=pageMax
    LASTPAGELIMIT=TOTALPAGE%pageMax
    NOPAGELIMIT=TOTALPAGE-LASTPAGELIMIT
    if(NOPAGELIMIT<curpage ):
        ENDPAGELIMIT=LASTPAGELIMIT
        pass
    STARTPAGE=(curpage//pageMax)*pageMax +1
    if curpage%pageMax==0 :
        #print 's'
        STARTPAGE=STARTPAGE-pageMax
        pass
    ENDPAGE=STARTPAGE+ENDPAGELIMIT -1
    
    
    return isVal,STARTPAGE,ENDPAGE,TOTALCOUNT,listdata



def testss():
    
    return 1




def render_to():
    def renderer(func):
        
        def wrapper(request, *args, **kw):
            output = func(request, *args, **kw)
            
            if isinstance(output, HttpResponse ):
                return output  
            elif isinstance(output, (list, tuple)):
                return render_to_response(output[1], output[0], RequestContext(request))
            
            elif isinstance(output, dict):
                return json_response( output )
            
            return output
        
        
        return wrapper
    
    
    return renderer

            
    

class myDict(dict):
    def update(self, *args):
        dict.update(self, *args)
        return self



def formerror_message( error_form ):
    
    errorhtml = "<ul>"

    for key, value in error_form.errors.items() :
        errorhtml = errorhtml + "<li><q>" + key + "</q>"+ value.as_text() +   "</li>"     
    
    errorhtml = errorhtml + "</ul>"
    
    return errorhtml 

def filenamedecode(encodestr):
    return urllib.unquote(encodestr.encode('utf-8'))


def content_file_name(instance, filename):
    #aa = urllib.quote(unicode(filename).encode('utf-8'))
    #print aa
    #print urllib.unquote(aa)
    return os.sep.join(['content', urllib.quote(unicode(filename).encode('utf-8')) ])



def choices_date_range():
    
    def ds(date):
        return date.strftime('%Y-%m-%d')
    
    today = datetime.date.today()
    today_week = today.weekday()
    year = today.year
    month = today.month
    
    year_minus = (today + datetime.timedelta(-calendar.mdays[today.month]) ).year
    month_minus = (today + datetime.timedelta(-calendar.mdays[today.month]) ).month
    
    year_plus = (today +  datetime.timedelta(calendar.mdays[today.month]) ).year
    month_plus = (today + datetime.timedelta(calendar.mdays[today.month]) ).month

        
    weeks = [ ( ds(today + datetime.timedelta(-today_week))+"," + ds(today + datetime.timedelta(-today_week+6)) , u'이번주'),
              ( ds(today + datetime.timedelta(-today_week-7))+"," + ds(today + datetime.timedelta(-today_week+6-7)) , u'지난주'),
              ( ds(today + datetime.timedelta(-today_week+7))+"," + ds(today + datetime.timedelta(-today_week+6+7)) , u'다음주'),
              
              ( ds(today )+"," + ds(today ) , u'오늘'),
              ( ds(today + datetime.timedelta(-1))+"," + ds(today + datetime.timedelta(-1)) , u'어제'),
              ( ds(today + datetime.timedelta(1))+"," + ds(today + datetime.timedelta(1)) , u'내일'),
              
              ( ds(datetime.date(year,month,1))+"," + ds(datetime.date(year,month,calendar.monthrange(year,month)[1] )) , u'이번달'),
              ( ds(datetime.date(year_minus,month_minus,1))+"," + ds(datetime.date(year_minus,month_minus,calendar.monthrange(year_minus,month_minus)[1])) , u'지난달'),
              ( ds(datetime.date(year_plus,month_plus,1))+"," + ds(datetime.date(year_plus,month_plus,calendar.monthrange(year_plus,month_plus)[1])) , u'다음달') ]
    
    return weeks



class Choices(object):
    """
    Convenience class for Django model choices which will use a [Small]IntegerField
    for storage.
    
        >>> statuses = Choices(
        ...     ('live', 'Live'),
        ...     ('draft', 'Draft'),
        ...     ('hidden', 'Not Live'),
        ... )
        >>> statuses.live
        0
        >>> statuses.hidden
        2
        >>> statuses.get_choices()
        ((0, 'Live'), (1, 'Draft'), (2, 'Not Live'))
    
    This is then useful for use in a model field with the choices attribute.
    
        >>> from django.db import models
        >>> class Entry(models.Model):
        ...     STATUSES = Choices(
        ...         ('live', 'Live'),
        ...         ('draft', 'Draft'),
        ...         ('hidden', 'Not Live'),
        ...     )
        ...     status = models.SmallIntegerField(choices=STATUSES.get_choices(),
        ...                                       default=STATUSES.live)
        
    It's also useful later when you need to filter by your choices.
    
        >>> live_entries = Entry.objects.filter(status=Entries.STATUSES.live)
                                              
    """
    
    def __init__(self, *args):
        super(Choices, self).__init__()
        self.__dict__['_keys'], self.__dict__['_values'] = zip(*args)
    
    def __getattr__(self, name):
        # have to iterate manually to avoid conversion to a list for < 2.6 compat
        for i, k in enumerate(self._keys):
            if k == name:
                return i
        raise AttributeError("No attribute %r." % name)
    
    def __setattr__(self, name, value):
        raise AttributeError("%r object does not support attribute assignment" % self.__class__.__name__)
    
    def __delattr__(self, name):
        raise AttributeError("%r object does not support attribute deletion" % self.__class__.__name__)
    
    def __getitem__(self, name):
        try:
            return self._values[getattr(self, name)]
        except AttributeError:
            raise KeyError(name)
    
    def __contains__(self, name):
        return name in self._keys
    
    def __repr__(self):
        return repr(self.items())
    
    def items(self):
        return tuple(zip(self._keys, self._values))
    
    def keys(self):
        # no need to copy since _keys is a tuple
        return self._keys
    
    def values(self):
        # no need to copy since _values is a tuple
        return self._values
    
    def get_choices(self):
        return tuple(enumerate(self._values))

# tests


    