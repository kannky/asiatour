from django.shortcuts import render
from django.http.response import HttpResponse

from asiatour.common.utils import render_to
from django.template.context_processors import request

# Create your views here.


@render_to()
def hotel(request):
    
    return {}, "content/hotel.html"

@render_to()
def golf(request):
    
    return {}, "content/golf.html"

@render_to()
def tour(request):
    
    return {}, "content/tour.html"

@render_to()
def packagetour(request):
    
    return {}, "content/packagetour.html"

@render_to()
def tourinformation(request):
    
    return {}, "content/tourinformation.html"

@render_to()
def ticket(request):
    
    return {}, "content/ticket.html"

@render_to()
def freetour(request):
    
    return {}, "content/freetour.html"

@render_to()
def access_terms(request):
    
    return {}, "content/access_terms.html"







